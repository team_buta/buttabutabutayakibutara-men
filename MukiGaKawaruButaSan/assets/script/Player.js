// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        player:{
            default:null,
        },
        front:{
            default:null,
            type:cc.Prefab,
        },
        right:{
            default:null,
            type:cc.Prefab,
        },
        left:{
            default:null,
            type:cc.Prefab,
        },
        back:{
            default:null,
            type:cc.Prefab,
        },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.player = cc.instantiate(this.right);
        this.node.addChild(this.player,3,"player");
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    },

    onKeyDown:function(event){
        console.log("key down");
        switch (event.keyCode) {
            case cc.macro.KEY.a:
                this.player = cc.instantiate(this.left);
                break;
            case cc.macro.KEY.d:
                this.player = cc.instantiate(this.right);
                break;
            case cc.macro.KEY.w:
                this.player = cc.instantiate(this.front);
                break;
            case cc.macro.KEY.s:
                this.player = cc.instantiate(this.back);
                break;
        }
        //今までの自分に別れを告げる
        this.node.getChildByName("player").destroy();
        //こんにちは。あたらしい自分
        this.node.addChild(this.player,3,"player");
    },
    start () {

    },

    // update (dt) {},
});
